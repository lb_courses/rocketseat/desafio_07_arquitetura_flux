// call, requisições com a api
// put, escuta actions do redux
// takeLatest, mesmo se clicar no btn varias vezes, vai executar apenas uma
import { call, put, all, takeLatest, select } from 'redux-saga/effects';
import { Alert } from 'react-native';
import api from '../../../services/api';
import { addToCartSuccess, updateAmountSuccess } from './actions';
import { formatPrice } from '../../../utils/format';
import NavigationService from '../../../services/navigation';

function* addToCart({ id }) {
  // verifica no state do carrinho se já existe o produto
  const productExists = yield select(state =>
    state.cart.find(p => p.id === id)
  );

  // verifica o estoque
  const stock = yield call(api.get, `/stock/${id}`);

  const stockAmount = stock.data.amount;
  // pega a quantidade de produtos que já tem no carrinho
  const currentAmount = productExists ? productExists.amount : 0;

  const amount = currentAmount + 1;

  // // verifica se a quantidade que ele quer e menor ou igual ao do estoque
  if (amount > stockAmount) {
    Alert.alert('Quantidade solicitada fora de estoque!');
    return;
  }

  if (productExists) {
    // const amount = productExists.amount + 1;

    // chama a action que já existe para atualizar o amount
    yield put(updateAmountSuccess(id, amount));
  } else {
    const response = yield call(api.get, `/products/${id}`);

    const data = {
      ...response.data,
      amount: 1,
      priceFormatted: formatPrice(response.data.price),
    };

    // yield put(addToCartSuccess(response.data));

    yield put(addToCartSuccess(data));
    NavigationService.navigate('Cart');
  }
}

function* updataAmount({ id, amount }) {
  if (amount <= 0) return;
  const stock = yield call(api.get, `stock/${id}`);
  const stockAmount = stock.data.amount;

  if (amount > stockAmount) {
    Alert.alert('Quantidade solicitada fora de estoque!');
    return;
  }

  yield put(updateAmountSuccess(id, amount));
}

// fica escutando as actions do redux
export default all([
  takeLatest('@cart/ADD_REQUEST', addToCart),
  takeLatest('@cart/UPDATE_AMOUNT_REQUEST', updataAmount),
]);
