import produce from 'immer';

export default function cart(state = [], action) {
  switch (action.type) {
    case '@cart/ADD_SUCCESS':
      // o draft é uma copia do state
      return produce(state, draft => {
        // const productIndex = draft.findIndex(p => p.id === action.product.id);

        // if (productIndex >= 0) {
        //   draft[productIndex].amount += 1;
        // } else {
        //   draft.push({
        //     ...action.product,
        //     amount: 1,
        //   });
        // }

        // remove toda a verificação da quantidade para o saga
        // apenas add o produto no carrinho
        const { product } = action;
        draft.push(product);
      });

    // return [
    //   ...state,
    //   {
    //     ...action.item,
    //     amount: 1,
    //   },
    // ];

    case '@cart/REMOVE':
      return produce(state, draft => {
        const productIndex = draft.findIndex(p => p.id === action.id);
        if (productIndex >= 0) {
          draft.splice(productIndex, 1);
        }
      });

    // case '@cart/UPDATE_AMOUNT': {
    case '@cart/UPDATE_AMOUNT_SUCCESS': {
      // if (action.amount <= 0) {
      //   return state;
      // }
      return produce(state, draft => {
        const productIndex = draft.findIndex(p => p.id === action.id);

        if (productIndex >= 0) {
          draft[productIndex].amount = Number(action.amount);
        }
      });
    }

    default:
      return state;
  }
}
