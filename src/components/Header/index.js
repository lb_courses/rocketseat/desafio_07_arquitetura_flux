import React from 'react';

import Icon from 'react-native-vector-icons/MaterialIcons';
// import { connect } from 'react-redux';
import { useSelector } from 'react-redux';
import { Wrapper, Container, Logo, BasketContainer, ItemCount } from './styles';

// function Header({ navigation, cartSize }) {
export default function Header({ navigation }) {
  const cartSize = useSelector(state => state.cart.length);
  return (
    <Wrapper>
      <Container>
        <Logo />
        <BasketContainer onPress={() => navigation.navigate('Cart')}>
          <Icon name="shopping-basket" color="#FFF" size={24} />
          <ItemCount>{cartSize || 0}</ItemCount>
        </BasketContainer>
      </Container>
    </Wrapper>
  );
}

// export default connect(state => ({
//   // cria uma propriedade nesse componente referente ao status cart do redux
//   cartSize: state.cart.length,
// }))(Header);
