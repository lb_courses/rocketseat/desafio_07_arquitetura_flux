import { RectButton } from 'react-native-gesture-handler';
import styled from 'styled-components/native';
import { darken } from 'polished';
import colors from '../../styles/colors';

export const Container = styled.View`
  background: ${colors.dark};
`;

export const Product = styled.View`
  background: #fff;
  width: 220px;
  padding: 10px;
  margin: 15px;
  border-radius: 5px;
`;

export const ProductImage = styled.Image`
  height: 200px;
  width: 200px;
`;

export const ProductTitle = styled.Text.attrs({
  numberOfLines: 2,
})`
  font-size: 16px;
`;
export const ProductPrice = styled.Text`
  font-size: 21px;
  font-weight: bold;
  margin: 5px 0 0 0;
`;

export const AddButton = styled(RectButton)`
  justify-content: center;
  align-content: center;
  flex-direction: row;
  margin-top: 5px;
  background: ${colors.primary};
  border-radius: 4px;
  opacity: ${props => (props.loading ? 0.7 : 1)};
`;

export const ProductAmount = styled.View`
  padding: 12px;
  background: ${colors.primary};
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px;
  background: ${darken(0.04, colors.primary)};
  flex-direction: row;
  align-items: center;
`;

export const ProductAmountText = styled.Text`
  color: #fff;
  margin: 0px 4px 0px 10px;
`;

export const AddButtonText = styled.Text`
  flex: 1;
  text-align: center;
  font-weight: bold;
  color: #fff;
  margin: auto;
`;
