import React, { useEffect, useState } from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { FlatList } from 'react-native';
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
import { useSelector, useDispatch } from 'react-redux';
import { formatPrice } from '../../utils/format';
import api from '../../services/api';
import * as CartActions from '../../store/modules/cart/actions';

import {
  Container,
  Product,
  ProductImage,
  ProductTitle,
  ProductPrice,
  AddButton,
  ProductAmount,
  ProductAmountText,
  AddButtonText,
} from './styles';

// class Home extends Component {
// function Home({ amount, addToCartRequest }) {
// export default function Home({ addToCartRequest }) {
export default function Home() {
  // state = {
  //   loading: false,
  // };

  const [products, setProducts] = useState([]);
  const amount = useSelector(state =>
    state.cart.reduce((sumAmount, product) => {
      sumAmount[product.id] = product.amount;
      return sumAmount;
    }, {})
  );

  const dispatch = useDispatch();
  // componentDidMount() {
  //   this.getProducts();
  // }

  // getProducts = async () => {
  //   const response = await api.get('/products');

  //   const data = response.data.map(product => ({
  //     ...product,
  //   }));

  //   this.setState({ products: data });
  // };

  useEffect(() => {
    async function loadProducts() {
      const response = await api.get('products');

      // executa a formatação assim que pega os dados da api
      // para executar apenas uma unica vez
      const data = response.data.map(product => ({
        ...product,
        priceFormatted: formatPrice(product.price),
      }));
      setProducts(data);
    }

    loadProducts();
  }, []);

  // handleAddProduct = product => {
  // handleAddProduct = id => {
  function handleAddProduct(id) {
    // this.setState({ loading: true });
    // const { dispatch } = this.props;
    // dispatch(CartActions.addToCart(product));

    // const { addToCart } = this.props;
    // addToCart(id);

    // const { addToCartRequest } = this.props;
    // addToCartRequest(id);
    dispatch(CartActions.addToCartRequest(id));
    // this.setState({ loading: false });
  }

  // renderProduct = ({ item }) => {
  function renderProduct({ item }) {
    // const { amount } = this.props;
    return (
      <Product>
        <ProductImage source={{ uri: item.image }} />
        <ProductTitle> {item.title} </ProductTitle>
        <ProductPrice>{formatPrice(item.price)}</ProductPrice>
        {/* <AddButton onPress={() => this.handleAddProduct(item.id)}> */}
        <AddButton onPress={() => handleAddProduct(item.id)}>
          <ProductAmount>
            <Icon name="add-shopping-cart" size={20} color="#FFF" />
            <ProductAmountText>{amount[item.id] || 0}</ProductAmountText>
          </ProductAmount>

          <AddButtonText>ADICIONAR</AddButtonText>
        </AddButton>
      </Product>
    );
  }

  // render() {

  // const { products } = this.state;

  return (
    <Container>
      <FlatList
        horizontal
        data={products}
        // extraData={this.props}
        extraData={amount}
        keyExtractor={item => String(item.id)}
        // renderItem={this.renderProduct}
        renderItem={renderProduct}
      />
    </Container>
  );
  // }

  // render() {
  //   const { loading } = this.state;
  //   return (
  //     <Container>
  //       <Product>
  //         <ProductImage source={require('./../../assets/shoe.webp')} />
  //         <ProductTitle> Women's Running Shoes. Nike.com</ProductTitle>
  //         <ProductPrice>R$ 123.12</ProductPrice>
  //         <AddButton loading={loading} onPress={this.handleAddUser}>
  //           <ProductAmount>
  //             <Icon name="add-shopping-cart" size={20} color="#FFF" />
  //             <ProductAmountText>0</ProductAmountText>
  //           </ProductAmount>
  //           <AddButtonText>ADICIONAR</AddButtonText>
  //         </AddButton>
  //       </Product>
  //     </Container>
  //   );
  // }
}

// const mapStateToProps = state => ({
//   amount: state.cart.reduce((amount, product) => {
//     amount[product.id] = product.amount;
//     return amount;
//   }, {}),
// });

// não precisa mais pega o dispath pelas props
// passa as propriedades direto pro component
// const mapDispatchToProps = dispatch =>
//   bindActionCreators(CartActions, dispatch);

// // export default connect(mapStateToProps, mapDispatchToProps)(Home);
// export default connect(null, mapDispatchToProps)(Home);

// Home.navigationOptions = {
//   title: 'Home',
// };
