import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
// import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
import { useDispatch, useSelector } from 'react-redux';
import { formatPrice } from '../../utils/format';
import * as CartActions from '../../store/modules/cart/actions';
import {
  Container,
  Products,
  Product,
  ProductImage,
  ProductInfo,
  ProductDetails,
  ProductTitle,
  ProductPrice,
  ProductDelete,
  ProductControllerPrice,
  ProductAmount,
  ProductControllerButton,
  ProductPriceTotal,
  ProductsDetails,
  ProductsTotalTitle,
  ProductsTotal,
  ProductsButton,
  ProductsButtonText,
  CartEmpty,
} from './styles';

// function Cart({ products, dispatch }) {
// function Cart({ products, removeFromCart, updateAmountRequest, total }) {
export default function Cart() {
  const total = useSelector(state =>
    formatPrice(
      state.cart.reduce((totalSum, product) => {
        return totalSum + product.price * product.amount;
      }, 0)
    )
  );

  const products = useSelector(state =>
    state.cart.map(product => ({
      ...product,
      // esses cálculos são feitos aqui pq só são atualizados se o status mudar.
      priceFormatted: formatPrice(product.price),
      subtotal: formatPrice(product.price * product.amount),
    }))
  );

  const dispatch = useDispatch();

  function increment(product) {
    // updateAmountRequest(product.id, product.amount + 1);
    dispatch(CartActions.updateAmountRequest(product.id, product.amount + 1));
  }

  function decrement(product) {
    dispatch(CartActions.updateAmountRequest(product.id, product.amount - 1));
  }

  console.tron.log('products cart => ', products);

  return (
    <Container>
      {products.length ? (
        <>
          <Products>
            {products.map(product => (
              <Product key={product.id}>
                <ProductInfo>
                  <ProductImage source={{ uri: product.image }} />
                  <ProductDetails>
                    <ProductTitle>{product.title}</ProductTitle>
                    <ProductPrice>{product.priceFormatted}</ProductPrice>
                  </ProductDetails>
                  <ProductDelete
                    onPress={() =>
                      // dispatch({ type: 'REMOVE_FROM_CART', id: product.id })
                      dispatch(CartActions.removeFromCart(product.id))
                    }
                  >
                    <Icon name="delete-forever" size={26} color="#7159c1" />
                  </ProductDelete>
                </ProductInfo>
                <ProductControllerPrice>
                  <ProductControllerButton onPress={() => decrement(product)}>
                    <Icon
                      name="remove-circle-outline"
                      size={26}
                      color="#7159c1"
                    />
                  </ProductControllerButton>
                  <ProductAmount value={String(product.amount)} />
                  <ProductControllerButton onPress={() => increment(product)}>
                    <Icon name="add-circle-outline" size={26} color="#7159c1" />
                  </ProductControllerButton>
                  <ProductPriceTotal>{product.subtotal}</ProductPriceTotal>
                </ProductControllerPrice>
              </Product>
            ))}
          </Products>

          <ProductsDetails>
            <ProductsTotalTitle>Total</ProductsTotalTitle>
            <ProductsTotal>{total}</ProductsTotal>
            <ProductsButton>
              <ProductsButtonText>FINALIZAR PEDIDO</ProductsButtonText>
            </ProductsButton>
          </ProductsDetails>
        </>
      ) : (
        <CartEmpty>
          <Icon name="remove-shopping-cart" size={64} color="#191920" />
          <ProductsTotalTitle>Seu carrinho está vazio!</ProductsTotalTitle>
        </CartEmpty>
      )}
    </Container>
  );
}

// pega os status do redux e tranforma em states do component
// const mapStateToProps = state => ({
//   // pega todas as infos do state cart do redux
//   products: state.cart.map(product => ({
//     ...product,
//     // esses cálculos são feitos aqui pq só são atualizados se o status mudar.
//     priceFormatted: formatPrice(product.price),
//     subtotal: formatPrice(product.price * product.amount),
//   })),
//   total: formatPrice(
//     state.cart.reduce((total, product) => {
//       return total + product.price * product.amount;
//     }, 0)
//   ),
// });

// const mapDispatchToProps = dispatch =>
//   bindActionCreators(CartActions, dispatch);

// export default connect(mapStateToProps, mapDispatchToProps)(Cart);
