import styled from 'styled-components/native';
import { RectButton } from 'react-native-gesture-handler';
import colors from '../../styles/colors';

export const Container = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
})`
  background: #fff;
  margin: 20px;
  padding: 10px;
  border-radius: 5px;
`;

export const Products = styled.View``;

export const Product = styled.View``;

export const ProductInfo = styled.View`
  flex-direction: row;
  align-items: center;
  /* flex-direction: row; */
  /* align-content: center; */
  justify-content: space-around;
  /* flex: 1; */
  /* align-content: center; */
  /* justify-content: center;  */
  padding: 10px;
`;
export const ProductImage = styled.Image`
  width: 80px;
  height: 80px;
  background: #cecece;
  border-radius: 4px;
`;

export const ProductDetails = styled.View`
  flex: 1;
  padding: 5px;
  /* margin: 15px; */
`;

export const ProductTitle = styled.Text.attrs({
  numberOfLines: 2,
})`
  font-size: 12px;
`;
export const ProductPrice = styled.Text`
  font-weight: bold;
  font-size: 17px;
  margin-top: 8px;
`;
export const ProductDelete = styled.TouchableOpacity`
  margin: auto;
`;
export const ProductControllerPrice = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background: rgba(0, 0, 0, 0.1);
  border-radius: 5px;
  margin: 5px;
  padding: 0 5px;
`;

export const ProductControllerButton = styled.TouchableOpacity``;

export const ProductAmount = styled.TextInput.attrs({
  readonly: true,
})`
  background: #fff;
  padding: 5px;
  margin: 0 5px;
  border: 1px solid #ddd;
  border-radius: 4px;
  min-width: 52px;
`;

export const ProductPriceTotal = styled.Text`
  font-weight: bold;
  font-size: 16px;
  flex: 1;
  text-align: right;
  margin-right: 10px;
`;

export const ProductsDetails = styled.View`
  padding-top: 20px;
  padding-bottom: 40px;
  align-items: center;
`;

export const ProductsTotalTitle = styled.Text``;

export const ProductsTotal = styled.Text`
  font-weight: bold;
  font-size: 25px;
`;

export const ProductsButton = styled(RectButton)`
  justify-content: center;
  align-content: center;
  flex-direction: row;
  margin-top: 20px;
  background: ${colors.primary};
  border-radius: 4px;
  width: 100%;
`;

export const ProductsButtonText = styled.Text`
  font-weight: bold;
  font-size: 18px;
  color: #fff;
  margin: 10px 0;
`;

export const CartEmpty = styled.View`
  flex-direction: column;
  align-items: center;
  margin-top: 50%;
  /* align-content: center; */

  /* justify-content: center; */
  /* font-weight: bold; */
`;
