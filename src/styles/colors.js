import { darken } from 'polished';

export default {
  primary: '#7159c1',
  dark: '#191920',
  header: darken(0.02, '#191920'),
};
